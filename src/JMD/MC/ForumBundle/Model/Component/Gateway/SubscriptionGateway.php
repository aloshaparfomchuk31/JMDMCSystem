<?php
namespace JMD\MC\ForumBundle\Model\Component\Gateway;

use Doctrine\ORM\QueryBuilder;
use JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface;
use JMD\MC\ForumBundle\Model\Component\Gateway\BaseGateway;
use JMD\MC\ForumBundle\Entity\Subscription;

class SubscriptionGateway extends BaseGateway implements GatewayInterface
{
    /**
     *
     * @access private
     * @var string $queryAlias
     */
    protected $queryAlias = 's';

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findSubscription(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        return $this->one($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findSubscriptions(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        return $this->all($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder $qb
     * @param  Array                      $parameters
     * @return int
     */
    public function countSubscriptions(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createCountQuery();
        }

        if (null == $parameters) {
            $parameters = array();
        }

        $qb->setParameters($parameters);

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Subscription                      $subscription
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function saveSubscription(Subscription $subscription)
    {
        $this->persist($subscription)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Subscription                      $subscription
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function updateSubscription(Subscription $subscription)
    {
        $this->persist($subscription)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Subscription                      $subscription
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function deleteSubscription(Subscription $subscription)
    {
        $this->remove($subscription)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Subscription
     */
    public function createSubscription()
    {
        return new $this->entityClass();
    }
}
