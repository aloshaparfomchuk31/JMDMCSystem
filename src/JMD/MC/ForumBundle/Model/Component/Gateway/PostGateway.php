<?php
namespace JMD\MC\ForumBundle\Model\Component\Gateway;

use Doctrine\ORM\QueryBuilder;
use JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface;
use JMD\MC\ForumBundle\Model\Component\Gateway\BaseGateway;
use JMD\MC\ForumBundle\Entity\Post;

class PostGateway extends BaseGateway implements GatewayInterface
{
    /**
     *
     * @access private
     * @var string $queryAlias
     */
    protected $queryAlias = 'p';

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findPost(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        return $this->one($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findPosts(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        $qb->addOrderBy('p.createdDate', 'ASC');

        return $this->all($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder $qb
     * @param  Array                      $parameters
     * @return int
     */
    public function countPosts(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createCountQuery();
        }

        if (null == $parameters) {
            $parameters = array();
        }

        $qb->setParameters($parameters);

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function savePost(Post $post)
    {
        $this->persist($post)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function updatePost(Post $post)
    {
        $this->persist($post)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function deletePost(Post $post)
    {
        $this->remove($post)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Post
     */
    public function createPost()
    {
        return new $this->entityClass();
    }
}
