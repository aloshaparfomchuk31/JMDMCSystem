<?php
namespace JMD\MC\ForumBundle\Model\Component\Manager;

use Doctrine\Common\Collections\ArrayCollection;

use JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface;
use JMD\MC\ForumBundle\Model\Component\Manager\BaseManager;

use JMD\MC\ForumBundle\Entity\Forum;

class ForumManager extends BaseManager implements ManagerInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Forum
     */
    public function createForum()
    {
        return $this->gateway->createForum();
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Forum $forum
     */
    public function saveForum(Forum $forum)
    {
        $this->gateway->saveForum($forum);

        return $this;
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Forum $forum
     */
    public function updateForum(Forum $forum)
    {
        $this->gateway->updateForum($forum);

        return $this;
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Forum $forum
     */
    public function deleteForum(Forum $forum)
    {
        // If we do not refresh the forum, AND we have reassigned the categories to null,
        // then its lazy-loaded categories are dirty, as the categories in memory will
        // still have the old category id set. Removing the forum will cascade into deleting
        // categories aswell, even though in the db the relation has been set to null.
        $this->refresh($forum);
        $this->remove($forum)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param \Doctrine\Common\Collections\ArrayCollection $categories
     * @param \JMD\MC\ForumBundle\Entity\Forum          $forum
     */
    public function reassignCategoriesToForum(ArrayCollection $categories, Forum $forum = null)
    {
        foreach ($categories as $category) {
            $category->setForum($forum);
            $this->persist($category);
        }

        $this->flush();

        return $this;
    }
}
