<?php
namespace JMD\MC\ForumBundle\Model\Component\Manager;

use Doctrine\ORM\QueryBuilder;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;

interface ManagerInterface
{
    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Model\FrontModel\ModelInterface             $model
     * @return \JMD\MC\ForumBundle\Model\Component\Repository\ManagerInterface
     */
    public function setModel(ModelInterface $model);

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Gateway\GatewayInterface
     */
    public function getGateway();

    /**
     *
     * @access public
     * @param  Array                                        $aliases = null
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function createSelectQuery(Array $aliases = null);

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function one(QueryBuilder $qb);

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder $qb
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function all(QueryBuilder $qb);

    /**
     *
     * @access public
     * @param  Object                                          $entity
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function persist($entity);

    /**
     *
     * @access public
     * @param  Object                                          $entity
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function remove($entity);

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function flush();

    /**
     *
     * @access public
     * @param  Object                                          $entity
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function refresh($entity);
}
