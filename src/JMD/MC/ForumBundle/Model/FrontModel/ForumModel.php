<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Doctrine\Common\Collections\ArrayCollection;
use JMD\MC\ForumBundle\Model\FrontModel\BaseModel;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Forum;

class ForumModel extends BaseModel implements ModelInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Forum
     */
    public function createForum()
    {
        return $this->getManager()->createForum();
    }

    /**
     *
     * @access public
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllForums()
    {
        return $this->getRepository()->findAllForums();
    }

    /**
     *
     * @access public
     * @param  string                              $forumName
     * @return \JMD\MC\ForumBundle\Entity\Forum
     */
    public function findOneForumById($forumId)
    {
        return $this->getRepository()->findOneForumById($forumId);
    }

    /**
     *
     * @access public
     * @param  string                              $forumName
     * @return \JMD\MC\ForumBundle\Entity\Forum
     */
    public function findOneForumByName($forumName)
    {
        return $this->getRepository()->findOneForumByName($forumName);
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Forum $forum
     */
    public function saveForum(Forum $forum)
    {
        return $this->getManager()->saveForum($forum);
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Forum $forum
     */
    public function updateForum(Forum $forum)
    {
        return $this->getManager()->updateForum($forum);
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Forum $forum
     */
    public function deleteForum(Forum $forum)
    {
        return $this->getManager()->deleteForum($forum);
    }

    /**
     *
     * @access public
     * @param \Doctrine\Common\Collections\ArrayCollection $categories
     * @param \JMD\MC\ForumBundle\Entity\Forum          $forum
     */
    public function reassignCategoriesToForum(ArrayCollection $categories, Forum $forum = null)
    {
        return $this->getManager()->reassignCategoriesToForum($categories, $forum);
    }
}
