<?php
namespace JMD\MC\ForumBundle\Component\TwigExtension;

use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;

class BoardListExtension extends \Twig_Extension
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\FrontModel\ModelInterface $categoryModel
     */
    protected $categoryModel;

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface $categoryManager
     */
    public function __construct(ModelInterface $categoryModel)
    {
        $this->categoryModel = $categoryModel;
    }

    /**
     *
     * @access public
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('board_list', array($this, 'boardList')),
        );
    }

    /**
     * Gets all boards available with their categories.
     *
     * @access public
     * @return array
     */
    public function boardList($forumName)
    {
        return $this->categoryModel->findAllCategoriesWithBoardsForForumByName($forumName);
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return 'boardList';
    }
}
