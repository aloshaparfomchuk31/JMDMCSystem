<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use JMD\MC\ForumBundle\Entity\Topic;

class ModeratorTopicResponseEvent extends UserTopicEvent
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\HttpFoundation\Response $response
     */
    protected $response;

    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request  $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @param \JMD\MC\ForumBundle\Entity\Topic        $topic
     */
    public function __construct(Request $request, Response $response, Topic $topic = null)
    {
        $this->request = $request;
        $this->response = $response;
        $this->topic = $topic;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
