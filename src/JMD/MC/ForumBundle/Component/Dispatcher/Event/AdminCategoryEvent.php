<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

use JMD\MC\ForumBundle\Entity\Category;

class AdminCategoryEvent extends Event
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\HttpFoundation\Request $request
     */
    protected $request;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Category $category
     */
    protected $category;

    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \JMD\MC\ForumBundle\Entity\Category    $category
     */
    public function __construct(Request $request, Category $category = null)
    {
        $this->request = $request;
        $this->category = $category;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}
