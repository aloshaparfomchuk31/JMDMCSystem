<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

use JMD\MC\ForumBundle\Entity\Board;
use JMD\MC\ForumBundle\Entity\Topic;

class ModeratorTopicMoveEvent extends Event
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\HttpFoundation\Request $request
     */
    protected $request;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Topic $topic
     */
    protected $topic;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Board $oldBoard
     */
    protected $oldBoard;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Board $newBoard
     */
    protected $newBoard;

    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \JMD\MC\ForumBundle\Entity\Topic       $topic
     */
    public function __construct(Request $request, Board $oldBoard, Board $newBoard, Topic $topic = null)
    {
        $this->request = $request;
        $this->topic = $topic;
        $this->oldBoard = $oldBoard;
        $this->newBoard = $newBoard;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function getOldBoard()
    {
        return $this->oldBoard;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function getNewBoard()
    {
        return $this->newBoard;
    }
}
