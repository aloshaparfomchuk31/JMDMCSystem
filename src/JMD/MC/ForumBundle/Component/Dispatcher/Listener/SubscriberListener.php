<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Listener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\UserTopicEvent;

class SubscriberListener implements EventSubscriberInterface
{
    /**
     *
     * @access private
     * @var \JMD\MC\ForumBundle\Model\FrontModel\SubscriptionModel $subscriptionModel
     */
    protected $subscriptionModel;

    /**
     *
     * @access protected
     * @var TokenStorageInterface $securityContext
     */
    protected $securityContext;

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Model\FrontModel\SubscriptionModel $subscriptionModel
     * @param TokenStorageInterface         $securityContext
     */
    public function __construct($subscriptionModel, TokenStorageInterface $securityContext)
    {
        $this->subscriptionModel = $subscriptionModel;
        $this->securityContext = $securityContext;
    }

    /**
     *
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            ForumEvents::USER_TOPIC_CREATE_COMPLETE      => 'onTopicCreateComplete',
            ForumEvents::USER_TOPIC_REPLY_COMPLETE       => 'onTopicReplyComplete'
        );
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Component\Dispatcher\Event\UserTopicEvent $event
     */
    public function onTopicCreateComplete(UserTopicEvent $event)
    {
        if ($event->getTopic()) {
            if ($event->getTopic()->getId() && $event->authorWantsToSubscribe()) {
                $user = $this->securityContext->getToken()->getUser()->getUserProxy();

                $this->subscriptionModel->subscribe($event->getTopic(), $user);
            }
        }
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Component\Dispatcher\Event\UserTopicEvent $event
     */
    public function onTopicReplyComplete(UserTopicEvent $event)
    {
        if ($event->getTopic()) {
            if ($event->getTopic()->getId()) {
                $user = $this->securityContext->getToken()->getUser()->getUserProxy();

                if ($event->authorWantsToSubscribe()) {
                    $this->subscriptionModel->subscribe($event->getTopic(), $user);
                }

                $subscriptions = $this->subscriptionModel->findAllSubscriptionsForTopicById($event->getTopic()->getId());

                $this->subscriptionModel->markTheseAsUnread($subscriptions, $user);
            }
        }
    }
}
