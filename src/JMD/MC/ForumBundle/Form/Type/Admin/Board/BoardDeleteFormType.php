<?php
namespace JMD\MC\ForumBundle\Form\Type\Admin\Board;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\True;
use Symfony\Component\Validator\Constraints\NotBlank;

class BoardDeleteFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $boardClass
     */
    protected $boardClass;

    /**
     *
     * @access public
     * @param string $boardClass
     */
    public function __construct($boardClass)
    {
        $this->boardClass = $boardClass;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $trueValidator = function (FormEvent $event) {
            $form = $event->getForm();

            $confirm = $form->get('confirm_delete')->getData();

            if (empty($confirm) || $confirm == false) {
                $form['confirm_delete']->addError(new FormError("You must confirm this action."));
            }
        };

        $builder
            ->add('confirm_delete', CheckboxType::class,
                array(
                    'mapped'             => false,
                    'required'           => true,
                    'label'              => 'board.confirm-delete-label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'constraints'        => array(
                        new IsTrue(),
                        new NotBlank()
                    ),
                )
            )
            ->add('confirm_subordinates', CheckboxType::class,
                array(
                    'mapped'             => false,
                    'required'           => true,
                    'label'              => 'board.confirm-delete-subordinates-label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'constraints'        => array(
                        new IsTrue(),
                        new NotBlank()
                    ),
                )
            )
            ->addEventListener(FormEvents::POST_SET_DATA, $trueValidator)
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->boardClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_board_delete_item',
            'validation_groups'   => array('forum_board_delete'),
            'cascade_validation'  => true,
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Forum_BoardDelete';
    }
}
