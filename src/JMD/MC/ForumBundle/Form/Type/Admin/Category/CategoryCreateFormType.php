<?php
namespace JMD\MC\ForumBundle\Form\Type\Admin\Category;

use JMD\MC\ForumBundle\Component\Helper\RoleHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CategoryCreateFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $categoryClass
     */
    protected $categoryClass;

    /**
     *
     * @access protected
     * @var string $forumClass
     */
    protected $forumClass;

    /**
     *
     * @access protected
     * @var RoleHelper $roleHelper
     */
    protected $roleHelper;

    /**
     *
     * @access public
     * @param string $categoryClass
     * @param string $forumClass
     * @param RoleHelper $roleHelper
     */
    public function __construct($categoryClass, $forumClass, RoleHelper $roleHelper)
    {
        $this->categoryClass = $categoryClass;
        $this->forumClass = $forumClass;
        $this->roleHelper = $roleHelper;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('forum', EntityType::class,
                array(
                    'class'              => $this->forumClass,
                    'query_builder'      =>
                        function (EntityRepository $er) {
                            return $er
                                ->createQueryBuilder('f')
                            ;
                        },
                    'data'               => $options['default_forum'],
                    'required'           => false,
                    'label'              => 'forum.label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('name', TextType::class,
                array(
                    'label'              => 'category.name-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('readAuthorisedRoles', ChoiceType::class,
                array(
                    'required'           => false,
                    'expanded'           => true,
                    'multiple'           => true,
                    'choices'            => $this->roleHelper->getRoleHierarchy(),
                    'label'              => 'category.roles.board-view-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->categoryClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_category_create_item',
            'validation_groups'   => array('forum_category_create'),
            'cascade_validation'  => true,
            'default_forum'       => null
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Forum_CategoryCreate';
    }
}
