<?php
namespace JMD\MC\ForumBundle\Form\Type\Admin\Category;

use JMD\MC\ForumBundle\Component\Helper\RoleHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryUpdateFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $categoryClass
     */
    protected $categoryClass;

    /**
     *
     * @access protected
     * @var string $forumClass
     */
    protected $forumClass;

    /**
     *
     * @access protected
     * @var RoleHelper $roleHelper
     */
    protected $roleHelper;

    /**
     *
     * @access public
     * @param string $categoryClass
     * @param string $forumClass
     * @param RoleHelper $roleHelper
     */
    public function __construct($categoryClass, $forumClass, RoleHelper $roleHelper)
    {
        $this->categoryClass = $categoryClass;
        $this->forumClass = $forumClass;
        $this->roleHelper = $roleHelper;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('forum', EntityType::class,
                array(
                    'class'              => $this->forumClass,
                    'query_builder'      =>
                        function (EntityRepository $er) {
                            return $er
                                ->createQueryBuilder('f')
                            ;
                        },
                    'required'           => false,
                    'label'              => 'forum.label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('name', TextType::class,
                array(
                    'label'              => 'category.name-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('readAuthorisedRoles', ChoiceType::class,
                array(
                    'required'           => false,
                    'expanded'           => true,
                    'multiple'           => true,
                    'choices'            => $options['available_roles'],
                    'label'              => 'category.roles.board-view-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->categoryClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_category_update_item',
            'validation_groups'   => array('forum_category_update'),
            'cascade_validation'  => true,
            'available_roles'     => $this->roleHelper->getRoleHierarchy(),
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Forum_CategoryUpdate';
    }
}
