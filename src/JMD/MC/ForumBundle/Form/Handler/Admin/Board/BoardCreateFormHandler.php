<?php
namespace JMD\MC\ForumBundle\Form\Handler\Admin\Board;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface ;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\AdminBoardEvent;
use JMD\MC\ForumBundle\Form\Handler\BaseFormHandler;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Category;
use JMD\MC\ForumBundle\Entity\Board;

class BoardCreateFormHandler extends BaseFormHandler
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Form\Type\Admin\Board\BoardCreateFormType $boardCreateFormType
     */
    protected $boardCreateFormType;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\FrontModel\BoardModel $boardModel
     */
    protected $boardModel;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Category $defaultCategory
     */
    protected $defaultCategory;

    /**
     *
     * @access public
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     * @param \Symfony\Component\Form\FormFactory                         $factory
     * @param \JMD\MC\ForumBundle\Form\Type\Admin\Board\BoardCreateFormType  $boardCreateFormType
     * @param \JMD\MC\ForumBundle\Model\FrontModel\BoardModel          $boardModel
     */
    public function __construct(EventDispatcherInterface $dispatcher, FormFactory $factory, $boardCreateFormType, ModelInterface $boardModel)
    {
        $this->dispatcher = $dispatcher;
        $this->factory = $factory;
        $this->boardCreateFormType = $boardCreateFormType;
        $this->boardModel = $boardModel;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                                          $forum
     * @return \JMD\MC\ForumBundle\Form\Handler\Admin\Category\CategoryCreateFormHandler
     */
    public function setDefaultCategory(Category $category)
    {
        $this->defaultCategory = $category;

        return $this;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\Form\Form
     */
    public function getForm()
    {
        if (null == $this->form) {
            $board = $this->boardModel->createBoard();

            $this->dispatcher->dispatch(ForumEvents::ADMIN_BOARD_CREATE_INITIALISE, new AdminBoardEvent($this->request, $board));

            $options = array(
                'default_category' => $this->defaultCategory
            );

            $this->form = $this->factory->create(get_class($this->boardCreateFormType), $board, $options);
        }

        return $this->form;
    }

    /**
     *
     * @access protected
     * @param \JMD\MC\ForumBundle\Entity\Board $board
     */
    protected function onSuccess(Board $board)
    {
        $this->dispatcher->dispatch(ForumEvents::ADMIN_BOARD_CREATE_SUCCESS, new AdminBoardEvent($this->request, $board));

        $this->boardModel->saveBoard($board);

        $this->dispatcher->dispatch(ForumEvents::ADMIN_BOARD_CREATE_COMPLETE, new AdminBoardEvent($this->request, $board));
    }
}
