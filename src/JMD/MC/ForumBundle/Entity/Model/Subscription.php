<?php
namespace JMD\MC\ForumBundle\Entity\Model;

use Symfony\Component\Security\Core\User\UserInterface;

use JMD\MC\ForumBundle\Entity\Forum as ConcreteForum;
use JMD\MC\ForumBundle\Entity\Topic as ConcreteTopic;

abstract class Subscription
{
    /** @var Topic $topic */
    protected $forum = null;

    /** @var Topic $topic */
    protected $topic = null;

    /** @var UserInterface $ownedBy */
    protected $ownedBy = null;

    /**
     *
     * @access public
     */
    public function __construct()
    {
        // your own logic
    }

    /**
     * Get topic
     *
     * @return Forum
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Set topic
     *
     * @param  Forum        $forum
     * @return Subscription
     */
    public function setForum(ConcreteForum $forum = null)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get topic
     *
     * @return Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set topic
     *
     * @param  Topic        $topic
     * @return Subscription
     */
    public function setTopic(ConcreteTopic $topic = null)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get owned_by
     *
     * @return UserInterface
     */
    public function getOwnedBy()
    {
        return $this->ownedBy;
    }

    /**
     * Set owned_by
     *
     * @param  UserInterface $ownedBy
     * @return Subscription
     */
    public function setOwnedBy($ownedBy = null)
    {
        $this->ownedBy = $ownedBy;

        return $this;
    }
}
