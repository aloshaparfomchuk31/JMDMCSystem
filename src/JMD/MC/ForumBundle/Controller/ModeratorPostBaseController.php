<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Post;

class ModeratorPostBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Post                                       $post
     * @return \JMD\MC\ForumBundle\Form\Handler\Moderator\Post\PostUnlockFormHandler
     */
    protected function getFormHandlerToUnlockPost(Post $post)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.post_unlock');

        $formHandler->setPost($post);
        $formHandler->setUser($this->getUser());
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }
}
