<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Forum;
use JMD\MC\ForumBundle\Entity\Board;
use JMD\MC\ForumBundle\Entity\Topic;

class UserTopicBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Board                        $board
     * @return \JMD\MC\ForumBundle\Form\Handler\TopicCreateFormHandler
     */
    protected function getFormHandlerToCreateTopic(Forum $forum, Board $board)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.topic_create');

        $formHandler->setForum($forum);
        $formHandler->setBoard($board);
        $formHandler->setUser($this->getUser());
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Topic                        $topic
     * @return \JMD\MC\ForumBundle\Form\Handler\TopicCreateFormHandler
     */
    protected function getFormHandlerToReplyToTopic(Topic $topic)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.post_create');

        $formHandler->setTopic($topic);
        $formHandler->setUser($this->getUser());
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }
}
