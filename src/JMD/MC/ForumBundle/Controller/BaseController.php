<?php
namespace JMD\MC\ForumBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\EventDispatcher\Event;

use JMD\MC\ForumBundle\Entity\Topic;
use JMD\MC\ForumBundle\Entity\Post;

class BaseController implements ContainerAwareInterface
{
    use ContainerAwareTrait;

	/**
     *
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router $router
     */
    private $router;

    /**
     *
     * @var \Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine $templating
     */
    private $templating;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\Request $request
     */
    protected $request;

    /**
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface  $dispatcher;
     */
    protected $dispatcher;

    /**
     *
     * @var \Symfony\Component\Security\Core\SecurityContext $securityContext
     */
    private $securityContext;

    /**
     *
     * @var \JMD\MC\ForumBundle\Component\Security\Authorizer $authorizer;
     */
    private $authorizer;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\ForumModel $forumModel
     */
    private $forumModel;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\CategoryModel $categoryModel
     */
    private $categoryModel;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\BoardModel $boardModel
     */
    private $boardModel;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\TopicModel $topicModel
     */
    private $topicModel;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\PostModel $postModel
     */
    private $postModel;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\RegistryModel $registryModel
     */
    private $registryModel;

    /**
     *
     * @var \JMD\MC\ForumBundle\Model\FrontModel\SubscriptionModel $subscriptionModel
     */
    private $subscriptionModel;

    /**
     *
     * @access protected
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected function getRouter()
    {
        if (null == $this->router) {
            $this->router = $this->container->get('router');
        }

        return $this->router;
    }

    /**
     *
     * @access protected
     * @param  string $route
     * @param  Array  $params
     * @return string
     */
    protected function path($route, $params = array())
    {
        return $this->getRouter()->generate($route, $params);
    }

    /**
     *
     * @access protected
     * @return \Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine
     */
    protected function getTemplating()
    {
        if (null == $this->templating) {
            $this->templating = $this->container->get('templating');
        }

        return $this->templating;
    }

    /**
     *
     * @access protected
     * @return \Symfony\Component\HttpFoundation\Request
     */
    protected function getRequest()
    {
        if (null == $this->request) {
            $this->request = $this->container->get('request_stack')->getCurrentRequest();
        }

        return $this->request;
    }

    /**
     *
     * @access protected
     * @param  string $prefix
     * @return array
     */
    protected function getCheckedItemIds($prefix = 'check_', $enforceNumericType = true)
    {
        $request = $this->getRequest();

        $sanitarisedIds = array();

        if ($request->request->has($prefix)) {
            $itemIds = $request->request->get($prefix);

            foreach ($itemIds as $id => $val) {
                if ($enforceNumericType == true) {
                    if (! is_numeric($id)) {
                        continue;
                    }
                }

                $sanitarisedIds[] = $id;
            }
        }

        return $sanitarisedIds;
    }

    /**
     *
     * @access protected
     * @param  string $template
     * @param  array  $params
     * @param  string $engine
     * @return string
     */
    protected function renderResponse($template, $params = array(), $engine = null)
    {
        return $this->getTemplating()->renderResponse($template . ($engine ?: $this->getEngine()), $params);
    }

    /**
     *
     * @access protected
     * @param  string                                             $url
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirectResponse($url)
    {
        return new RedirectResponse($url);
    }

    /**
     *
     * @access protected
     * @param  string                                             $forumName
     * @param  \JMD\MC\ForumBundle\Entity\Topic                $topic
     * @param  \JMD\MC\ForumBundle\Entity\Post                 $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirectResponseForTopicOnPageFromPost($forumName, Topic $topic, Post $post)
    {
        //$page = $this->getTopicModel()->getPageForPostOnTopic($topic, $topic->getLastPost()); // Page of the last post.
        $response = $this->redirectResponse($this->path('jmdmc_forum_user_topic_show', array(
            'forumName' => $forumName,
            'topicId' => $topic->getId(),
            /*'page' => $page*/
        )) /* . '#' . $topic->getLastPost()->getId()*/);

        return $response;
    }

    /**
     *
     * @access protected
     * @return string
     */
    protected function getEngine()
    {
        return $this->container->getParameter('jmdmc_forum.template.engine');
    }

    /**
     * @return \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     */
    protected function getSecurityContext()
    {
        if (null == $this->securityContext) {
            $this->securityContext = $this->container->get('security.authorization_checker');
        }

        return $this->securityContext;
    }

    /**
     *
     * @access protected
     * @return \Symfony\Component\Security\Core\User\UserInterface
     */
    protected function getUser()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        return $user != 'anon.' ? $user->getUserProxy() : null;
    }

    /**
     *
     * @access protected
     * @param  string $role
     * @return bool
     */
    protected function isGranted($role)
    {
        if (! $this->getSecurityContext()->isGranted($role)) {
            return false;
        }

        return true;
    }

    /**
     *
     * @access protected
     * @param  string                                                           $role|boolean $role
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    protected function isAuthorised($role)
    {
        if (is_bool($role)) {
            if ($role == false) {
                throw new AccessDeniedException('You do not have permission to use this resource.');
            }

            return true;
        }

        if (! $this->isGranted($role)) {
            throw new AccessDeniedException('You do not have permission to use this resource.');
        }

        return true;
    }

    /**
     *
     * @access protected
     * @param  \Object                                                      $item
     * @param  string                                                       $message
     * @return bool
     * @throws NotFoundHttpException
     */
    protected function isFound($item, $message = null)
    {
        if (null == $item) {
            throw new NotFoundHttpException($message ?: 'Page you are looking for could not be found!');
        }

        return true;
    }

    protected function getQuery($query, $default)
    {
        return $this->getRequest()->query->get($query, $default);
    }

    protected function dispatch($name, Event $event)
    {
        if (! $this->dispatcher) {
            $this->dispatcher = $this->container->get('event_dispatcher');
        }

        $this->dispatcher->dispatch($name, $event);
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Component\Security\Authorizer
     */
    protected function getAuthorizer()
    {
        if (null == $this->authorizer) {
            $this->authorizer = $this->container->get('jmdmc_forum.component.security.authorizer');
        }

        return $this->authorizer;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\ForumModel
     */
    protected function getForumModel()
    {
        if (null == $this->forumModel) {
            $this->forumModel = $this->container->get('jmdmc_forum.model.forum');
        }

        return $this->forumModel;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\CategoryModel
     */
    protected function getCategoryModel()
    {
        if (null == $this->categoryModel) {
            $this->categoryModel = $this->container->get('jmdmc_forum.model.category');
        }

        return $this->categoryModel;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\BoardModel
     */
    protected function getBoardModel()
    {
        if (null == $this->boardModel) {
            $this->boardModel = $this->container->get('jmdmc_forum.model.board');
        }

        return $this->boardModel;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\TopicModel
     */
    protected function getTopicModel()
    {
        if (null == $this->topicModel) {
            $this->topicModel = $this->container->get('jmdmc_forum.model.topic');
        }

        return $this->topicModel;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\PostModel
     */
    protected function getPostModel()
    {
        if (null == $this->postModel) {
            $this->postModel = $this->container->get('jmdmc_forum.model.post');
        }

        return $this->postModel;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\RegistryModel
     */
    protected function getRegistryModel()
    {
        if (null == $this->registryModel) {
            $this->registryModel = $this->container->get('jmdmc_forum.model.registry');
        }

        return $this->registryModel;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Model\FrontModel\SubscriptionModel
     */
    protected function getSubscriptionModel()
    {
        if (null == $this->subscriptionModel) {
            $this->subscriptionModel = $this->container->get('jmdmc_forum.model.subscription');
        }

        return $this->subscriptionModel;
    }

    /**
     *
     * @access protected
     */
    protected function getCrumbs()
    {
        return $this->container->get('jmdmc_forum.component.crumb_builder');
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Component\Helper\PaginationConfigHelper
     */
    protected function getPageHelper()
    {
        return $this->container->get('jmdmc_forum.component.helper.pagination_config');
    }
}
