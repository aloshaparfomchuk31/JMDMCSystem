<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Board;

class AdminBoardBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\Admin\Board\BoardCreateFormHandler
     */
    protected function getFormHandlerToCreateBoard($categoryFilter = null)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.board_create');

        $formHandler->setRequest($this->getRequest());

        if ($categoryFilter) {
            $category = $this->getCategoryModel()->findOneCategoryById($categoryFilter);

            if ($category) {
                $formHandler->setDefaultCategory($category);
            }
        }

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\BoardUpdateFormHandler
     */
    protected function getFormHandlerToUpdateBoard(Board $board)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.board_update');

        $formHandler->setRequest($this->getRequest());

        $formHandler->setBoard($board);

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\BoardDeleteFormHandler
     */
    protected function getFormHandlerToDeleteBoard(Board $board)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.board_delete');

        $formHandler->setRequest($this->getRequest());

        $formHandler->setBoard($board);

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Board $board
     * @return array
     */
    protected function getFilterQueryStrings(Board $board)
    {
        $params = array();

        if ($board->getCategory()) {
            $params['category_filter'] = $board->getCategory()->getId();

            if ($board->getCategory()->getForum()) {
                $params['forum_filter'] = $board->getCategory()->getForum()->getId();
            }
        }

        return $params;
    }

    /**
     *
     * @access protected
     * @return array
     */
    protected function getNormalisedCategoryAndForumFilters()
    {
        $forumFilter = $this->getQuery('forum_filter', null);
        $categoryFilter = $this->getQuery('category_filter', null);

        if ($categoryFilter) { // Corrective Measure incase forum/category filters fall out of sync.
            if ($category = $this->getCategoryModel()->findOneCategoryById($categoryFilter)) {
                if ($category->getForum()) {
                    $forumFilter = $category->getForum()->getId();
                } else {
                    $forumFilter = null; // Force it to be blank so 'unassigned' is highlighted.
                }
            } else {
                $forumFilter = null;
            }
        }

        return array(
            'forum_filter' => $forumFilter,
            'category_filter' => $categoryFilter
        );
    }
}
