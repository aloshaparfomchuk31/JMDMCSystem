<?php

namespace JMD\MC\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class ProjectInitCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('jmd:project:init')
            ->setDescription('Initial project command');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getHelper('question');
        $settings = $this->getContainer()->get('jmd.mc_core.settings');

        $questions = [
            'brand'          => new Question('Your site brand ['.gethostname().']: ', gethostname()),
            'copyright_name' => new Question('Copyright name ['.get_current_user().']: ', get_current_user()),
            'copyright_url'  => new Question('Copyright url [http://localhost]: ', 'http://localhost'),
            'contact_email'  => new Question('Your contact email ['.get_current_user().'@'. gethostname().']: ',
                get_current_user() . '@' . gethostname()),
            'avatar_size'    => new Question('Maximum avatar dimension [200x200]', '200x200'),
            'email_confirmation' => new ConfirmationQuestion('Enable email confirmation [false]?: ', false)
        ];

        $answers = [];
        foreach ($questions as $key => $question) {
            $answers[$key] = $questionHelper->ask($input, $output, $question);
        }

        foreach ($answers as $name => $value) {
            $settings->add($name, is_bool($value) ? ($value ? 1 : 0) : $value);
        }
    }
}
