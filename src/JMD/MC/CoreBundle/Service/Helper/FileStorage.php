<?php
namespace JMD\MC\CoreBundle\Service\Helper;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMD\MC\CoreBundle\Document\FileStorage as FileStorageDocument;
use JMD\MC\CoreBundle\Exception\FileStorageException;
use JMD\MC\CoreBundle\Service\FileManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;

class FileStorage
{
    /** @var FileManager */
    private $uploadableManager;

    /** @var DocumentManager */
    private $odm;

    /** @var Filesystem */
    private $filesystem;

    /** @var array */
    private $allowedMimeTypes = [];

    /** @var null */
    private $maxSize = null;

    /**
     * FileStorage constructor.
     * @param FileManager $uploadableManager
     * @param DocumentManager $odm
     * @param Filesystem $filesystem
     */
    public function __construct(
        FileManager $uploadableManager,
        DocumentManager $odm,
        Filesystem $filesystem
    ) {
        $this->uploadableManager = $uploadableManager;
        $this->odm = $odm;
        $this->filesystem = $filesystem;
    }

    /**
     * Upload file
     * @param UploadedFile $uploadedFile
     * @param null $document
     * @return FileStorageDocument
     * @throws FileStorageException
     */
    public function uploadFile(UploadedFile $uploadedFile, $document = null)
    {
        if (!$this->isAllowedMimeType($uploadedFile)) {
            throw new FileStorageException('Not allowed file mime-type!');
        }

        if ($this->getMaxSize() !== null && $this->getMaxSize() < $uploadedFile->getClientSize()) {
            throw new FileStorageException('File have too big size! Allowed max size is: '.$this->getMaxSize());
        }

        $fileStorage = new FileStorageDocument();
        $fileStorage
            ->setDocument($document)
            ->setOriginalName($uploadedFile->getClientOriginalName())
        ;

        $this->odm->persist($fileStorage);
        $this->uploadableManager->setFileStorage($fileStorage);
        $this->uploadableManager->upload($uploadedFile);

        $this->odm->persist($fileStorage);
        $this->odm->flush();

        return $fileStorage;
    }

    /**
     * Remove file
     * @param FileStorageDocument $fileStorage
     */
    public function removeFile(FileStorageDocument $fileStorage)
    {
        if ($this->filesystem->exists($fileStorage->getPath())) {
            $this->filesystem->remove($fileStorage->getPath());
        }

        $this->odm->remove($fileStorage);
        $this->odm->flush();
    }

    /**
     * @param array $allowedMimeTypes
     * @return FileStorage
     */
    public function setAllowedMimeTypes($allowedMimeTypes)
    {
        $this->allowedMimeTypes = $allowedMimeTypes;
        return $this;
    }

    /**
     * @return array
     */
    public function getAllowedMimeTypes()
    {
        return $this->allowedMimeTypes;
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    private function isAllowedMimeType(UploadedFile $file)
    {
        $type = $file->getClientMimeType();
        $allowedMimeTypes = $this->getAllowedMimeTypes();

        return count($allowedMimeTypes) > 0 ? in_array($type, $allowedMimeTypes) : true;
    }

    /**
     * @return int
     */
    public function getMaxSize()
    {
        return $this->maxSize;
    }

    /**
     * @param int $maxSize
     * @return $this
     */
    public function setMaxSize($maxSize)
    {
        $this->maxSize = $maxSize;

        return $this;
    }
}