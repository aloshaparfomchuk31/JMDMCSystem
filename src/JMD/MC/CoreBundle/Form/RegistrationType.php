<?php

namespace JMD\MC\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'registration.name'
            ])
            ->add('username', TextType::class, [
                'label' => 'registration.username'
            ])
            ->add('email', EmailType::class, [
                'label' => 'registration.email'
            ])
            ->add('password', PasswordType::class, [
                'label' => 'registration.password'
            ])
            ->add('retypePassword', PasswordType::class, [
                'mapped'   => false,
                'required' => true,
                'label' => 'registration.retypePassword'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'JMD\MC\CoreBundle\Document\User',
            'translation_domain' => 'security'
        ]);
    }
}
