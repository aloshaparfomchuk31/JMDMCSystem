<?php
namespace JMD\MC\CoreBundle\Form\Type;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMD\MC\CoreBundle\Form\DataTransformer\DocumentsToPropertyTransformer;
use JMD\MC\CoreBundle\Form\DataTransformer\DocumentToPropertyTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class Select2DocumentType extends AbstractType
{
    /** @var DocumentManager */
    private $dm;
    /** @var  RouterInterface */
    private $router;
    /** @var integer */
    private $pageLimit;
    /** @var integer */
    private $minimumInputLength;
    /** @var boolean */
    private $allowClear;
    /** @var integer */
    private $delay;
    /** @var  string */
    private $language;
    /** @var boolean */
    private $cache;

    /**
     * Select2DocumentType constructor.
     * @param DocumentManager $dm
     * @param RouterInterface $router
     * @param int $pageLimit
     * @param int $minimumInputLength
     * @param bool $allowClear
     * @param int $delay
     * @param string $language
     * @param bool $cache
     */
    public function __construct(
        DocumentManager $dm,
        RouterInterface $router,
        $pageLimit,
        $minimumInputLength,
        $allowClear,
        $delay,
        $language,
        $cache
    ) {
        $this->dm = $dm;
        $this->router = $router;
        $this->pageLimit = $pageLimit;
        $this->minimumInputLength = $minimumInputLength;
        $this->allowClear = $allowClear;
        $this->delay = $delay;
        $this->language = $language;
        $this->cache = $cache;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add custom data transformer
        if ($options['transformer']) {
            if (!is_string($options['transformer'])) {
                throw new \Exception('The option transformer must be a string');
            }
            if (!class_exists($options['transformer'])) {
                throw new \Exception('Unable to load class: ' . $options['transformer']);
            }

            $transformer = new $options['transformer']($this->dm, $options['class']);

            if (!$transformer instanceof DataTransformerInterface) {
                throw new \Exception(sprintf('The custom transformer %s must implement "Symfony\Component\Form\DataTransformerInterface"',
                    get_class($transformer)));
            }

            // add the default data transformer
        } else {
            $transformer = $options['multiple']
                ? new DocumentsToPropertyTransformer($this->dm, $options['class'], $options['text_property'],
                    $options['primary_key'])
                : new DocumentToPropertyTransformer($this->dm, $options['class'], $options['text_property'],
                    $options['primary_key']);
        }

        $builder->addViewTransformer($transformer, true);
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        // make variables available to the view
        $view->vars['remote_path'] = $options['remote_path']
            ?: $this->router->generate($options['remote_route'],
                array_merge($options['remote_params'], ['page_limit' => $options['page_limit']]));

        $varNames = array(
            'multiple',
            'minimum_input_length',
            'placeholder',
            'language',
            'allow_clear',
            'delay',
            'language',
            'cache',
            'primary_key'
        );
        foreach ($varNames as $varName) {
            $view->vars[$varName] = $options[$varName];
        }

        if ($options['multiple']) {
            $view->vars['full_name'] .= '[]';
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'class' => null,
                'primary_key' => 'id',
                'remote_path' => null,
                'remote_route' => null,
                'remote_params' => [],
                'multiple' => false,
                'compound' => false,
                'minimum_input_length' => $this->minimumInputLength,
                'page_limit' => $this->pageLimit,
                'allow_clear' => $this->allowClear,
                'delay' => $this->delay,
                'text_property' => null,
                'placeholder' => '',
                'language' => $this->language,
                'required' => false,
                'cache' => $this->cache,
                'transformer' => null,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'select2document';
    }
}