<?php
namespace JMD\MC\CoreBundle\Namer;

class Transliterator implements FilenameGeneratorInterface
{
    /**
     * @param $filename
     * @param $extension
     * @param null $object
     * @return mixed|string
     */
    public function generate($filename, $extension, $object = null)
    {
        $filename = $this->translitIt($filename);
        $date = new \DateTime('NOW');
        return $filename . '_' . $date->format('d-m-Y_H-i-s') . $extension;
    }

    private function translitIt($string)
    {
        $langCodes = [
            // upper case
            'А' => 'A',     'Б' => 'B',     'В' => 'V',     'Г' => 'G',
            'Д' => 'D',     'Е' => 'E',     'Ё' => 'E',     'Ж' => 'Zh',
            'З' => 'Z',     'И' => 'I',     'Й' => 'I',     'К' => 'K',
            'Л' => 'L',     'М' => 'M',     'Н' => 'N',     'О' => 'O',
            'П' => 'P',     'Р' => 'R',     'С' => 'S',     'Т' => 'T',
            'У' => 'U',     'Ф' => 'F',     'Х' => 'Kh',    'Ц' => 'Tc',
            'Ч' => 'Ch',    'Ш' => 'Sh',    'Щ' => 'Shch',  'Ъ' => '',
            'Ы' => 'Y',     'Ь' => '',      'Э' => 'E',     'Ю' => 'Iu',
            'Я' => 'Ia',
            // lower case
            'а' => 'a',     'б' => 'b',     'в' => 'v',     'г' => 'g',
            'д' => 'd',     'е' => 'e',     'ё' => 'e',     'ж' => 'zh',
            'з' => 'z',     'и' => 'i',     'й' => 'i',     'к' => 'k',
            'л' => 'l',     'м' => 'm',     'н' => 'n',     'о' => 'o',
            'п' => 'p',     'р' => 'r',     'с' => 's',     'т' => 't',
            'у' => 'u',     'ф' => 'f',     'х' => 'kh',    'ц' => 'tc',
            'ч' => 'ch',    'ш' => 'sh',    'щ' => 'shch',  'ъ' => '',
            'ы' => 'y',     'ь' => '',      'э' => 'e',     'ю' => 'iu',
            'я' => 'ia',    ' ' => '_'
        ];

        $language = array(
            'from' => array_keys($langCodes),
            'to' => array_values($langCodes),
        );

        return str_replace($language['from'], $language['to'], $string);
    }
}
