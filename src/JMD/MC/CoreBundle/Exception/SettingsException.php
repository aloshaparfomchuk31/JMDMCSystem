<?php
namespace JMD\MC\CoreBundle\Exception;

use Exception;

class SettingsException extends \Exception
{
    public function __construct($message = "", array $parameters = [], $code = 0, Exception $previous = null)
    {
        foreach ($parameters as $name => $value) {
            $message = str_replace('%'.$name.'%', $value, $message);
        }
        parent::__construct($message, $code, $previous);
    }

}