<?php
namespace JMD\MC\CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;

/**
 * Class Settings
 * @package JMD\MC\CoreBundle\Document
 *
 * @ODM\Document()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Settings
{
    use SoftDeleteableDocument;

    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var string
     * @ODM\ReferenceOne(nullable=true)
     */
    private $refId;

    /**
     * @var string
     * @ODM\Field(type="string", nullable=false)
     */
    private $name;

    /**
     * @var mixed
     * @ODM\Field
     */
    private $value;

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refId
     *
     * @param $refId
     * @return self
     */
    public function setRefId($refId = null)
    {
        $this->refId = $refId;
        return $this;
    }

    /**
     * Get refId
     *
     * @return $refId
     */
    public function getRefId()
    {
        return $this->refId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }
}
