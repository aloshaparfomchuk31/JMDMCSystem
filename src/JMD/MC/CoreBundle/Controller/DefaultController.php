<?php

namespace JMD\MC\CoreBundle\Controller;

use JMD\MC\CoreBundle\Document\User;
use JMD\MC\CoreBundle\Entity\UserProxy;
use JMD\MC\CoreBundle\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends BaseController
{
    public function indexAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        if($error !== null) {
            $this->get('session')->getFlashBag()->add('danger', $this->trans(
                $error->getMessageKey(),
                $error->getMessageData(),
                'security'
            ));
        }

        $response = $this->forward('JMDMCNewsBundle:News:index');

        return $response;
    }

    public function registrationFormAction(Request $request)
    {
        $formData = $this->get('session')->get('formData');

        $form = $this->createForm(RegistrationType::class, null);

        if ($formData !== null && is_string($formData)) {
            $formData = json_decode($formData, 1);
            $user = new User();
            $user
                ->setName($formData['name'])
                ->setUsername($formData['username'])
                ->setEmail($formData['email'])
            ;

            $form->setData($user);
            unset($user);
        }

        $form->handleRequest($request);

        return $this->render('registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function registrationCheckAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                $passwordsNotEqual = false;
                if ($form->get('password')->getViewData() != $form->get('retypePassword')->getViewData()) {
                    $passwordsNotEqual = true;
                    $this->get('session')->getFlashBag()->add('danger', $this->get('translator')->trans(
                        'registration.errors.passwordsNotEqual',
                        [],
                        'security'
                    ));
                }

                if (!$form->isValid() || $passwordsNotEqual) {
                    $errors = $form->getErrors();
                    foreach ($errors as $error) {
                        $this->get('session')->getFlashBag()->add('danger', $error->getMessage());
                    }

                    $this->get('session')->set('formData', json_encode($request->get('registration'), 1));
                } else {
                    $uuid = $this->get('jmd.mc_core.uuid')->offlineUserConvert($user->getUsername());

                    $userProxy = new UserProxy();
                    $userProxy
                        ->setUsername($user->getUsername())
                        ->setUuid($uuid)
                    ;

                    $this->getDoctrine()->getManager()->persist($userProxy);
                    $this->getDoctrine()->getManager()->flush();

                    $confirmationEnabled = $this->get('jmd.mc_core.settings')->get('email_confirmation');

                    $sessionMessage = null;
                    if ($confirmationEnabled !== null && $confirmationEnabled->getValue() == 0) {
                        $user->setEnabled(true);
                        $sessionMessage = $this->get('translator')->trans(
                            'registration.success',
                            [],
                            'security'
                        );
                    } else {
                        $code = uniqid(mt_rand());
                        
                        $emailHelper = $this->get('jmd.mc_core.mailer.helper');
                        $emailHelper
                            ->setSubject($this->trans('registration.message.confirmation_subject', [
                                '%hostname%' => $request->getHttpHost()
                            ], 'security'))
                            ->setMessage($this->trans('registration.message.email_confirm', [
                                '%url%' => $this->get('router')->getGenerator()->generate('jmdmc_core_confirmation_check', [
                                    'code' => $code
                                ], UrlGeneratorInterface::ABSOLUTE_URL)
                            ], 'security'))
                            ->sendEmail($user->getEmail(), ':Email:email_confirm.html.twig')
                        ;

                        $user
                            ->setConfirmationCode($code)
                            ->setUuid($uuid)
                        ;

                        $sessionMessage = $this->get('translator')->trans(
                            'registration.confirm_email',
                            [],
                            'security'
                        );
                    }

                    $user->setMysqlId($userProxy->getId());
                    $this->getODM()->persist($user);
                    $userProxy->setMongoId($user->getId());
                    $this->getODM()->persist($userProxy);
                    $this->getODM()->flush();

                    $this->get('session')->getFlashBag()->add('info', $sessionMessage);
                    $this->get('session')->remove('formData');
                }
            }
        }

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @param $code
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function confirmationCheckAction($code, Request $request)
    {
        $user = $this->getODM()->getRepository('JMDMCCoreBundle:User')->findOneBy([
            'confirmationCode'  => $code
        ]);

        $referer = $request->headers->get('referer') ?
            $request->headers->get('referer') : $this->get('router')->generate('jmdmc_core_homepage');

        $sessionMessage = $this->trans('registration.confirmation_success', [], 'security');
        if (!$user) {
            $sessionMessage = $this->trans('registration.confirmation_bad_code', [], 'security');
            $this->get('session')->getFlashBag()->add('warning', $sessionMessage);

            return $this->redirect($referer);
        }

        $this->get('session')->getFlashBag()->add('info', $sessionMessage);

        $user->setEnabled(true);
        $this->getODM()->flush($user);

        return $this->redirect($referer);
    }

    public function recoverPasswordAction($step = '1', Request $request)
    {
        if ($this->isGranted('ROLE_USER')) {
            $this->get('session')->getFlashBag()->add('danger', $this->trans('recover.logged.in', [], 'security'));
            return $this->redirectToRoute('jmdmc_core_homepage');
        }

        switch ($step) {
            case '1':
                if ($request->getMethod() === 'POST') {
                    $user = $this->getODM()->getRepository('JMDMCCoreBundle:User')->loadUserByUsername($request->get('username'));
                    if (null === $user) {
                        $this->get('session')->getFlashBag()->add('danger', $this->trans('user_not_found', [], 'security'));
                        break;
                    }

                    $code = uniqid(mt_rand());

                    $emailHelper = $this->get('jmd.mc_core.mailer.helper');
                    $emailHelper
                        ->setSubject($this->trans('recover.recover_subject', [
                            '%hostname%' => $request->getHttpHost()
                        ], 'security'))
                        ->setMessage($this->trans('recover.recover_confirm', [
                            '%url%' => $this->get('router')->getGenerator()->generate('jmdmc_core_recover_password', [
                                'step' => '2',
                                'code' => $code,
                            ], UrlGeneratorInterface::ABSOLUTE_URL)
                        ], 'security'))
                        ->sendEmail($user->getEmail(), ':Email:email_recover.html.twig');

                    $user->setConfirmationCode($code);

                    $this->getODM()->persist($user);
                    $this->getODM()->flush();

                    $this->get('session')->getFlashBag()->add('info', $this->trans('recover.code_send_success', [], 'security'));
                    return $this->redirectToRoute('jmdmc_core_homepage');
                }
                break;
            case '2':
                $code = $request->get('code');
                $user = $this->getODM()->getRepository('JMDMCCoreBundle:User')->findOneBy([
                    'confirmationCode'  => $code
                ]);

                if (null === $user) {
                    $this->get('session')->getFlashBag()->add('danger',
                        $this->trans('recover.bad_code', [], 'security'));
                    return $this->redirectToRoute('jmdmc_core_homepage');
                }

                if ($request->getMethod() === 'POST') {
                    $password = $request->get('password');
                    $retypedPassword = $request->get('retypePassword');

                    if ($password != $retypedPassword) {
                        $this->get('session')->getFlashBag()->add('danger',
                            $this->trans('recover.passwords_not_equal', [], 'security'));
                        break;
                    }

                    $user->setSalt(uniqid(mt_rand(), true));
                    $user->setConfirmationCode(uniqid(mt_rand(), true));
                    $password = $this->get('security.password_encoder')->encodePassword($user, $password);
                    $user
                        ->setPassword($password)
                        ->setLastPasswordUpdateDate(new \DateTime('NOW'))
                    ;

                    $this->getODM()->persist($user);
                    $this->getODM()->flush();

                    $this->get('session')->getFlashBag()->add('info',
                        $this->trans('recover.success', [], 'security'));

                    return $this->redirectToRoute('jmdmc_core_homepage');
                }

                break;
        }

        return $this->render('recover.html.twig', [
            'title' => $this->trans('title.recover_password')
        ]);
    }
}
