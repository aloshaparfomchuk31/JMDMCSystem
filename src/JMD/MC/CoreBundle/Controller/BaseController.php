<?php
namespace JMD\MC\CoreBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    /**
     * @return DocumentManager
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * Translate
     * @param string $key
     * @param array $params
     * @param string $domain
     * @return string
     */
    public function trans($key, array $params = [], $domain = 'messages')
    {
        return $this->get('translator')->trans($key, $params, $domain);
    }

    public function transChoice($key, $number, array $params = [], $domain = 'messages')
    {
        return $this->get('translator')->transChoice($key, $number, $params, $domain);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    public function getSession()
    {
        return $this->get('session');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface|\Symfony\Component\HttpFoundation\Session\SessionBagInterface
     */
    public function getFlashBag()
    {
        return $this->getSession()->getFlashBag();
    }

    protected function render($view, array $parameters = array(), Response $response = null)
    {
        if (!stristr($view, ':')) {
            $className = static::class;
            $classReflection = new \ReflectionClass($className);
            $shortName = $classReflection->getShortName();
            $directory = str_replace('Controller', '', $shortName);

            $view = ':' . $directory . ':' . $view;
        }

        return parent::render($view, $parameters, $response);
    }

    public function getControllers()
    {
        $routes = $this->container->get('router')->getRouteCollection()->all();

        $controllers = [];
        foreach ($routes as $name => $route) {
            $defaults = $route->getDefaults();

            if (isset($defaults['_controller'])
                && preg_match('/(.*)\\\(.*Bundle)\\\Controller\\\(.*)Controller::(.*)Action/', $defaults['_controller'])
                && isset($defaults['can_be_start_page'])
            ) {
                $controllers[] = preg_replace_callback(
                    '/(.*)\\\(.*Bundle)\\\Controller\\\(.*)Controller::(.*)Action/',
                    function ($matches) {
                        $controllerString = str_replace('\\', '', $matches[1]);
                        $controllerString .= $matches[2];
                        $controllerString .= ':' . $matches[3];
                        $controllerString .= ':' . $matches[4];
                        return $controllerString;
                    },
                    $defaults['_controller']
                );
            }
        }

        return $controllers;
    }

    /**
     * Redirect to referer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToReferer()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        return $this->redirect($request->headers->get('referer'));
    }
}
