<?php

namespace JMD\MC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use JMD\MC\CoreBundle\Document\User;

/**
 * UserProxy
 *
 * @ORM\Table(name="user_proxy")
 * @ORM\Entity(repositoryClass="JMD\MC\CoreBundle\Entity\Repository\UserProxyRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UserProxy
{
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @Gedmo\ReferenceOne(type="document", class="JMD\MC\CoreBundle\Document\User", mappedBy="userProxy", identifier="mongoId")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="mongo_id", type="string", nullable=false)
     */
    protected $mongoId;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $uuid;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return UserProxy
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return UserProxy
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * Set mongoId
     *
     * @param string $mongoId
     *
     * @return UserProxy
     */
    public function setMongoId($mongoId)
    {
        $this->mongoId = $mongoId;

        return $this;
    }

    /**
     * Get mongoId
     *
     * @return string
     */
    public function getMongoId()
    {
        return $this->mongoId;
    }
}
