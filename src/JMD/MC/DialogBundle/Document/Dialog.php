<?php
namespace JMD\MC\DialogBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use JMD\MC\CoreBundle\Document\User;

/**
 * Class Dialog
 * @package JMD\MC\DialogBundle\Document
 *
 * @ODM\Document()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Dialog
{
    use SoftDeleteableDocument;
    use TimestampableDocument;

    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var User[]
     * @ODM\ReferenceMany(targetDocument="JMD\MC\CoreBundle\Document\User")
     */
    private $users;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $theme;

    /**
     * @var Message[]
     *
     * @ODM\ReferenceMany(targetDocument="Message", mappedBy="dialog")
     */
    private $messages;

    /**
     * @var User
     * @ODM\ReferenceOne(targetDocument="JMD\MC\CoreBundle\Document\User")
     */
    private $creator;

    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add user
     *
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return User[] $users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add message
     *
     * @param Message $message
     */
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;
    }

    /**
     * Remove message
     *
     * @param Message $message
     */
    public function removeMessage(Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return Message[] $messages
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set creator
     *
     * @param User $creator
     * @return self
     */
    public function setCreator(User $creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * Get creator
     *
     * @return User $creator
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param string $theme
     *
     * @return self
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }
}
