<?php

namespace JMD\MC\NewsBundle\Controller;

use JMD\MC\CoreBundle\Controller\BaseController;
use JMD\MC\CoreBundle\Exception\ValidationException;
use JMD\MC\NewsBundle\Bus\Command\RemoveCommentCommand;
use JMD\MC\NewsBundle\Document\Comment;
use JMD\MC\NewsBundle\Document\News;
use JMD\MC\NewsBundle\Form\CommentType;
use JMD\MC\NewsBundle\Form\NewsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends BaseController
{
    public function indexAction(Request $request)
    {
        $qb = $this->getODM()->getRepository('JMDMCNewsBundle:News')->createQueryBuilder();

        $qb
            ->addAnd($qb->expr()->field('enabled')->equals(true))
            ->sort('createdAt', 'desc')
        ;

        $request = $request->createFromGlobals();

        if ($request->get('tag', false)) {
            $qb->addAnd($qb->expr()->field('tags')->in([ $request->get('tag') ]));
        }

        $news = $this->get('knp_paginator')->paginate(
            $qb,
            $request->get('page', 1),
            10
        );

        return $this->render('index.html.twig', [
            'title' => $this->trans('title.news.index'),
            'news'  => $news
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Security("is_granted('ROLE_USER')")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(NewsType::class);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if (!$form->isValid()) {
                $errors = $form->getErrors();
                foreach ($errors as $error) {
                    $this->getFlashBag()->add('danger', $error->getMessage());
                }

                return $this->render('create.html.twig', [
                    'title' => $this->trans('title.news.create'),
                    'form' => $form->createView(),
                ]);
            }
            
            $news = $form->getData();

            $news->setUser($this->getUser());

            $this->getODM()->persist($news);
            $this->getODM()->flush();

            $this->addFlash('success', $this->trans('news.success.create', [], 'news'));

            return $this->redirectToRoute('jmdmc_news_user_list');
        }

        return $this->render('create.html.twig', [
            'title' => $this->trans('title.news.create'),
            'form'  => $form->createView(),
        ]);
    }

    /**
     * @param News $news
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(News $news, Request $request)
    {
        if (!$news->getEnabled() && $this->getUser() !== $news->getUser()) {
            if (!$this->isGranted('ROLE_MODERATOR')) {
                throw $this->createNotFoundException();
            }
        }

        $form = $this->createForm(CommentType::class, null, [
            'value' => $news
        ]);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if (!$form->isValid()) {
                $errors = $form->getErrors();
                foreach ($errors as $error) {
                    $this->getFlashBag()->add('danger', $error->getMessage());
                }

                return $this->render('view.html.twig', [
                    'title' => $this->trans('title.news.view', [
                        '%title%' => $news->getTitle()
                    ]),
                    'news' => $news,
                    'form' => $form->createView()
                ]);
            }

            $comment = $form->getData();
            $comment->setUser($this->getUser());

            $this->getODM()->persist($comment);
            $this->getODM()->flush();

            return $this->redirectToRoute('jmdmc_news_view', [ 'id' => $news->getId() ]);
        }

        return $this->render('view.html.twig', [
            'title' => $this->trans('title.news.view', [
                '%title%' => $news->getTitle()
            ]),
            'news'  => $news,
            'form'  => $form->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     */
    public function listAction(Request $request)
    {
        $news = $this->getODM()->getRepository('JMDMCNewsBundle:News')->createQueryBuilder();

        if ($this->isGranted('ROLE_MODERATOR') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            $news->field('enabled')->equals('false');
        } elseif (!$this->isGranted('ROLE_MODERATOR') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            $news->field('user')->references($this->getUser());
        }

        $news = $this->get('knp_paginator')->paginate(
            $news->sort('updatedAt', 'desc'),
            $request->get('page', 1),
            25
        );

        return $this->render('list.html.twig', [
            'title' => $this->trans('title.news.user_list'),
            'news'  => $news
        ]);
    }

    public function removeAction(News $news)
    {
        if ($news->getEnabled() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->createAccessDeniedException();
        }

        $this->getODM()
            ->getRepository('JMDMCNewsBundle:Comment')
            ->createQueryBuilder()
            ->findAndRemove()
            ->field('news')
            ->references($news)
            ->getQuery()
            ->execute();

        $this->getODM()->remove($news);
        $this->getODM()->flush();

        return $this->redirectToReferer();
    }

    public function switchStatusAction(News $news)
    {
        if (!$this->isGranted('ROLE_MODERATOR')) {
            return $this->createAccessDeniedException();
        }
        
        $news->setEnabled(!$news->getEnabled());
        
        $this->getODM()->persist($news);
        $this->getODM()->flush();
        
        $this->addFlash(
            $news->getEnabled() ? 'success' : 'danger',
            $this->transChoice(
                'news.status.changed',
                $news->getEnabled() ? 1 : 0,
                [ '%title%' => $news->getTitle() ],
                'news'
            ));

        return $this->redirectToReferer();
    }

    public function editAction(News $news, Request $request)
    {
        if ($news->getEnabled() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->createAccessDeniedException();
        }

        $form = $this->createForm(NewsType::class, $news);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if (!$form->isValid()) {
                $errors = $form->getErrors();
                foreach ($errors as $error) {
                    $this->getFlashBag()->add('danger', $error->getMessage());
                }

                return $this->render('create.html.twig', [
                    'title' => $this->trans('title.news.edit', ['%title%' => $news->getTitle()]),
                    'form' => $form->createView()
                ]);
            }

            $this->getODM()->persist($news);
            $this->getODM()->flush();

            $this->addFlash('success', $this->trans('news.success.edit', [], 'news'));

            return $this->redirectToReferer();
        }

        return $this->render('create.html.twig', [
            'title' => $this->trans('title.news.edit', [ '%title%' => $news->getTitle() ]),
            'form'  => $form->createView()
        ]);
    }

    public function removeCommentAction(Comment $comment)
    {
        try {
            $command = new RemoveCommentCommand([], [ 'id' => $comment->getId() ]);
            $this->get('command_bus')->handle($command);
            $this->addFlash('success', $this->trans('news.success.remove.comment', [], 'news'));
        } catch (ValidationException $e) {
            $this->addFlash('danger', $e->getMessages());
        } catch (\Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }

        return $this->redirectToReferer();
    }
}
