<?php
namespace JMD\MC\NewsBundle\Form;

use JMD\MC\CoreBundle\Form\Type\DocumentHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'label'         => 'news.form.content',
                'attr'          => [
                    'rows'      => 5
                ]
            ])
            ->add('news', DocumentHiddenType::class, [
                'class'         => 'JMD\MC\NewsBundle\Document\News',
                'data_class'    => null,
                'data'          => $options['value'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'            => 'JMD\MC\NewsBundle\Document\Comment',
            'value'                 => null,
            'translation_domain'    => 'news',
        ]);
    }

}