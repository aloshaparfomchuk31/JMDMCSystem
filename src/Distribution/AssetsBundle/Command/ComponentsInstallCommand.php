<?php

namespace Distribution\AssetsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ComponentsInstallCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('components:install')
            ->setDefinition(array(
                new InputArgument('target', InputArgument::OPTIONAL, 'The target directory', 'web'),
            ))
            ->addOption('symlink', null, InputOption::VALUE_NONE, 'Symlinks the components instead of copying it')
            ->addOption('relative', null, InputOption::VALUE_NONE, 'Make relative symlinks')
            ->setDescription('Installs bundles web assets under a public web directory')
            ->setHelp(<<<EOT
The <info>%command.name%</info> command installs components assets into a given
directory (e.g. the <comment>web</comment> directory).

  <info>php %command.full_name% web</info>

A "bundles" directory will be created inside the target directory and the
"Resources/public" directory of each bundle will be copied into it.

To create a symlink to each bundle instead of copying its assets, use the
<info>--symlink</info> option:

  <info>php %command.full_name% web --symlink</info>

To make symlink relative, add the <info>--relative</info> option:

  <info>php %command.full_name% web --symlink --relative</info>

EOT
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $DS        = DIRECTORY_SEPARATOR;
        $targetArg = rtrim($input->getArgument('target'), $DS);

        if (!is_dir($targetArg)) {
            throw new \InvalidArgumentException(sprintf(
                'The target directory "%s" does not exist.',
                $input->getArgument('target')
            ));
        }

        if (!function_exists('symlink') && $input->getOption('symlink')) {
            throw new \InvalidArgumentException(
                'The symlink() function is not available on your system. '
                . 'You need to install the assets without the --symlink option.'
            );
        }

        $filesystem    = $this->getContainer()->get('filesystem');
        $rootDir       = $this->preparePath($this->getContainer()->get('kernel')->getRootDir());
        $rootDir       = $rootDir . $DS . '..';
        $componentsDir = $rootDir . $DS . $this->preparePath($targetArg . $DS . 'plugins');

        $filesystem->mkdir($componentsDir, 0777);

        $output->writeln(sprintf(
            'Installing components as <comment>%s</comment>',
            $input->getOption('symlink') ? 'symlinks' : 'hard copies'
        ));

        $components = $this->getContainer()->getParameter('distribution_assets.components');

        foreach ($components as $name => $path) {
            $originDir = $this->preparePath($rootDir . $DS . rtrim($path, $DS));

            if (is_dir($originDir)) {
                $targetDir = $componentsDir . $DS . $name;

                $output->writeln(sprintf(
                    'Installing components for <comment>%s</comment> into <comment>%s</comment>',
                    $name,
                    str_replace($rootDir, '', $targetDir)
                ));

                $filesystem->remove($targetDir);

                if ($input->getOption('symlink')) {
                    if ($input->getOption('relative')) {
                        $relativeOriginDir = $filesystem->makePathRelative($originDir, realpath($componentsDir));
                    } else {
                        $relativeOriginDir = $originDir;
                    }

                    $filesystem->symlink($relativeOriginDir, $targetDir);
                } else {
                    $filesystem->mkdir($targetDir, 0777);
                    // We use a custom iterator to ignore VCS files
                    $filesystem->mirror(
                        $originDir,
                        $targetDir,
                        Finder::create()->ignoreDotFiles(false)->in($originDir)
                    );
                }
            } else {
                $output->writeln(sprintf('Path <comment>%s</comment> not found. <error>Skip.</error>', $originDir));
            }
        }
    }

    /**
     * @param string $path
     * @return mixed|string
     */
    private function preparePath($path)
    {
        return str_replace(['\\', '/'], [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR], $path);
    }
}
